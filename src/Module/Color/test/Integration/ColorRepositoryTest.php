<?php

namespace LaSalle\ChupiProject\Module\Color\test\Integration;

use LaSalle\ChupiProject\Module\Color\Domain\RandomColorSearcher;
use LaSalle\ChupiProject\Module\Color\test\Infraestructure\Stub\ColorsStub;
use PHPUnit\Framework\TestCase;

/** test */
final class InMemoryColorRepositoryTest extends TestCase
{
    public function test_it_should_return_something_if_repository_is_ok(){

        $stub = ColorsStub::full();
        $wordSearcher  = new RandomColorSearcher($stub);
        $word = $wordSearcher();
        $this->assertNotNull( $word );

    }
    public function test_it_should_return_null_if_repository_is_empty(){

        $stub = ColorsStub::empty();
        $wordSearcher  = new RandomColorSearcher($stub);
        $word = $wordSearcher();
        $this->assertNull( $word );


    }
}

