<?php

namespace LaSalle\ChupiProject\Module\Color\test\Behaviour;


use LaSalle\ChupiProject\Module\Color\Domain\RandomColorSearcher;
use LaSalle\ChupiProject\Module\Color\test\Infraestructure\Stub\ColorsStub;
use Mockery;
use PHPUnit\Framework\TestCase;

/** test */
final class RandomColorSearcherTest extends TestCase
{

    public function test_dependecy_which_is_repository_is_invoked_with_all_function(){
        //setup
        $aaa     =  Mockery::mock('LaSalle\ChupiProject\Module\Color\Domain\ColorRepository');
        $randomColorSearcher  = new RandomColorSearcher( $aaa);

        //definir que le pasara al mock
        $aaa->shouldReceive('all')
            ->withNoArgs();

        //lamada a la funcion

        $result = $randomColorSearcher();
        self::assertNull($result);
    }

    /**
     *
     */
    public function test_it_should_be_random(){

        $cyanCount = 0;
        $magentaCount = 0;
        $stub = ColorsStub::full();
        $wordSearcher  = new RandomColorSearcher($stub);
        for($i = 0 ; $i < 10000 ; $i++){
            $word = $wordSearcher();

            if($word === "cyan"){
                $cyanCount++;
            }else {
                $magentaCount++;

            }
        }
        $this->assertGreaterThan(0.9,$cyanCount/$magentaCount);
        $this->assertGreaterThan(0.9,$magentaCount/$cyanCount);

    }

}

