<?php

namespace LaSalle\ChupiProject\Module\Color\test\Infraestructure\Stub;

use LaSalle\ChupiProject\Module\Color\Domain\ColorRepository;


final class ColorStub implements ColorRepository
{
    /**
     * ColorStub constructor.
     * @param $isEmpty
     */
    public function __construct($isEmpty){

        if($isEmpty === "empty"){
            $this->colors = [];
        }else{
            $this->colors = ["cyan","magenta"];
        }

    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->colors;
    }
}
