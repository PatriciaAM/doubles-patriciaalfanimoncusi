<?php
namespace LaSalle\ChupiProject\Module\Color\test\Infraestructure\Stub;

use LaSalle\ChupiProject\Module\Color\test\Infraestructure\Stub\ColorStub;

class ColorsStub
{

    /**
     * @return \LaSalle\ChupiProject\Module\Color\test\Infraestructure\Stub\ColorStub
     */
    static function empty()
    {
        $stub = new ColorStub("empty");
        return $stub;

    }


    /**
     * @return \LaSalle\ChupiProject\Module\Color\test\Infraestructure\Stub\ColorStub
     */
    static function full()
    {
        $stub = new ColorStub("full");
        return $stub;
    }

}
