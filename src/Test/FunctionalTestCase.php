<?php
/**
 * Created by PhpStorm.
 * User: caprica
 * Date: 25/03/17
 * Time: 14:36
 */
namespace LaSalle\ChupiProject\Test;

require 'chupi.php';


use LaSalle\ChupiProject\Module\Color\test\Infraestructure\Stub\ColorsStub;
use PHPUnit\Framework\TestCase;

/** test */
final class FunctionalTestCase extends TestCase
{
    public function test_it_should_be_random(){

        $stub = ColorsStub::full();
        $wordSearcher  = new RandomColorSearcher($stub);

        for($i = 0 ; $i < 10000 ; $i++){
            $word = _random_color_except($wordSearcher,"cyan");
            $this->assertEquals("magenta",$word);
        }
    }

}
